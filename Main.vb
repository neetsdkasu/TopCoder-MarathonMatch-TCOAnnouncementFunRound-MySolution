Imports System

Public Module Main

    Public Sub Main()
        Try
            Dim S As Integer = CInt(Console.ReadLine())
            Dim data(S - 1) As String
            For i As Integer = 0 To S - 1
                data(i) = Console.ReadLine()
            Next i

            Dim wf As New WaterfallFishing()
            Dim ret() As Integer = wf.placeTraps(data)
            
            Console.WriteLine(ret.Length)
            For Each r As Integer In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()
            
        Catch ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub
    
End Module