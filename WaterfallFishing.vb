Imports System
Imports System.Collections.Generic
Imports Tpl = System.Tuple(Of Integer, Double)

Public Class WaterfallFishing
    Private Class Heap(Of V)
        Dim cmp As Comparison(Of V), values() As V, count As Integer
        Sub New(size As Integer, c As Comparison(Of V))
            ReDim values(size): cmp = c
        End Sub
        Sub Push(val As V)
            values(count) = val: count += 1: GoUp(count - 1)
        End SUb
        Function Pop() As V
            Dim t As V = values(0): count -= 1
            values(0) = values(count): GoDown(0): Return t
        End Function
        Sub Beam(size As Integer)
            If count > size Then count = size
        End Sub
        Function IsEmpty() As Boolean
            Return count = 0
        End Function
        Sub Swap(i As Integer, j As Integer)
            Dim t As V = values(i): values(i) = values(j): values(j) = t
        End Sub
        Sub GoUp(i As Integer)
            If i = 0 Then
                GoDown(i): Exit Sub
            End If
            Dim j As Integer = (i - 1) \ 2
            If cmp(values(i), values(j)) < 0 Then
                Swap(i, j): GoUp(j)
            Else
                GoDown(i)
            End If
        End Sub
        Sub GoDown(i As Integer)
            Dim j As Integer = i * 2 + 1
            Dim k As Integer = j + 1
            If j >= count Then Exit SUb
            If k < count Then
                If  cmp(values(j), values(k)) > 0 Then j = k
            End IF
            If cmp(values(i), values(j)) > 0 Then
                Swap(i, j): GoDown(j)
            End If
        End Sub
    End Class
    Public Function placeTraps(data As String()) As Integer()
        Dim D As Integer = data.Length
        Dim W As Integer = data(0).Length
        Dim valData(D - 1, W - 1) As Integer
        Dim traps As New HashSet(Of Integer)()
        For i As Integer = 0 To D - 1
            For j As Integer = 0 To W - 1
                If Char.IsDigit(data(i)(j)) Then
                    valData(i, j) = (Asc(data(i)(j)) - Asc("0"))
                Else
                    valData(i, j) = (Asc(data(i)(j)) - Asc("A") + 10)
                End If
            Next j
        Next i
        Dim pq As New Heap(Of Tpl)(W, Function(a As Tpl, b As Tpl)
            Return b.Item2.CompareTo(a.Item2)
        End Function)
        Dim pq2 As New Heap(Of Tpl)(W * 20, Function(a As Tpl, b As Tpl)
            Return b.Item2.CompareTo(a.Item2)
        End Function)
        Dim dw As Integer = (W + 3) \ 4
        Dim dw2 As Integer = dw \ 2
        For k As Integer = 0 To 7
            pq.Beam(0)
            For z As Integer = 0 To dw
                Dim j As Integer = k * dw2 + z
                If j < 0 OrElse j >= W Then Continue For
                Dim mx As Integer = 0
                For i As Integer = 0 To D - 1
                    mx = Math.Max(mx, valData(i, j))
                Next i
                pq.Push(New Tpl(j, mx))
            Next z
            For i As Integer = 0 To 3
                If pq.IsEmpty() Then Exit For
                pq2.Push(pq.Pop())
            Next i
        Next k
        For i As Integer = 0 To Math.Max(1, W \ 5)
            If pq2.IsEmpty() Then Exit For
            traps.Add(pq2.Pop().Item1)
        Next i
        If traps.Count = 0 Then
            pq.Beam(0)
            For j As Integer = 0 To W - 1
                Dim mx As Integer = 0
                For i As Integer = 0 To D - 1
                    mx = Math.Max(mx, valData(i, j))
                Next i
                pq.Push(New Tpl(j, mx))
            Next j
            For i As Integer = 0 To Math.Max(1, W \ 6)
                If pq.IsEmpty() Then Exit For
                traps.Add(pq.Pop().Item1)
            Next i
        End If
        placeTraps = traps.ToArray()
    End Function
End Class